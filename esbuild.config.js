const esbuild = require('esbuild')

// Automatically exclude all node_modules from the bundled version
const { nodeExternalsPlugin } = require('esbuild-node-externals')
const { typecheckPlugin } = require('@jgoz/esbuild-plugin-typecheck');

const config = {
  // esbuild configuration option - same as the CLI parameters
  entryPoints: ['./src/index.ts'],
  outfile: 'build/index.js',
  bundle: true,
  minify: true,
  platform: 'node',
  sourcemap: true,
  target: 'node16',
  plugins: [nodeExternalsPlugin(), typecheckPlugin()]
}

esbuild.build(config).catch(() => process.exit(1))