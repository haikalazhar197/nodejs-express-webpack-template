import express, { Request, Response } from "express";

const router = express.Router();

router.get("/", (req: Request, res: Response) => {
  console.log("Hello There");

  // const a = {
  //   b: "1"
  // }

  // a.b = 1;

  res.status(200).json({ message: "Hello World Hello" });
});

export default router;
